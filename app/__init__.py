from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
from flask_mail import Mail
import os

db = SQLAlchemy()
migrate = Migrate()
login_manager = LoginManager()
jwt = JWTManager()
mail = Mail()

def create_app(configuration):

    app = Flask(__name__)
    app.config.from_object(configuration)

    # Debug = True
    
    app.config["MAIL_SERVER"] = 'smtp.gmail.com'
    app.config["MAIL_PORT"] = 465
    app.config["MAIL_USE_SSL"] = True
    app.config["SQLALCHEMY_COMMIT_ON_TEARDOWN"] = True
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
    app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://gabriel:1234@localhost:5432/simpleteste"
    app.config["MAIL_USERNAME"] = os.getenv('MAIL_USERNAME')
    app.config["MAIL_PASSWORD"] = os.getenv('MAIL_PASSWORD')
    app.config["SECRET_KEY"] = 'secretkey'

    from app.views import app_routes
    app.register_blueprint(app_routes)

    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)
    jwt.init_app(app)
    mail.init_app(app)

    return app
