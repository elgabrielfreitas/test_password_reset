from app.config import Configuration
from app import create_app, mail
from flask_mail import Message

app = create_app(Configuration)

@app.route('/')
def index():
    sender = 'bielfreitascontato@gmail.com'
    recipients = ['bielfreitascontato@gmail.com']
    msg = Message()
    msg.subject = "Test Send"
    msg.recipients = recipients
    msg.sender = sender
    msg.body = 'testing'

    mail.send(msg)

if __name__ == '__main__':
    app.run()
